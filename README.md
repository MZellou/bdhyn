BdHyN
Application développée sous R (https://cran.r-project.org/)
avec le package Shiny (https://shiny.rstudio.com/)

Hébergement Rstudio : https://nt-dataviz-apps.shinyapps.io/bdhyn/

Mise en forme et exploration des données hydrobiologiques produites en Normandie dans le cadre du suivi Directive Cadre Européenne sur l'Eau.
Données produites par le laboratoire d'hydrobiologie de la DREAL Normandie et par des prestataires des agences de l'eau Loire-Bretagne et Seine-Normandie